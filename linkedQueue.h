/* 
 * Author: Evan Pierzina
 * 
 * linkedStack.h
 * ===========================================
 * This is the implementation of a custom linkedQueue that makes use of nodes
 * holding sub-queues (as arrays) that are linked together.  The size of the
 * sub-queue is determined by SIZE.  The purpose of this custom data structure
 * is to be able to work with a queue that holds large amounts of data but doesn't
 * require a large amount of contiguous space without incurring the overhead
 * from a ton of pointers.
 */

#ifndef LINKEDQUEUE_H
#define	LINKEDQUEUE_H
#define SIZE 32000
#include <cstdlib>
#include <iostream>


template <class myType>
struct queueNode {
    myType dataSet[SIZE];
    int front, back;
    queueNode<myType> *link;
};

template <class myType>
class linkedQueue {
public:
    linkedQueue();
    virtual ~linkedQueue();
    const bool isEmptyQueue();
    void initializeQueue();
    void enqueue(const myType& newItem);
    myType front();
    myType back();
    myType dequeue();
    int queueCount();
private:
    queueNode<myType> *queueFront;
    queueNode<myType> *queueRear;
    int count;
};


// linkedQueue constructor
// ====================================
// This function initializes all values of a new linkedQueue.

template <class myType>
linkedQueue<myType>::linkedQueue() {
    queueFront = NULL;
    queueRear = NULL;
    count = 0;
}


// linkedQueue destructor
// ====================================
// Calling this function deletes all references used in the linkedQueue
// and deallocates any used memory.

template <class myType>
linkedQueue<myType>::~linkedQueue() {
    initializeQueue();
}


// isEmptyQueue
// =====================================
// This function checks to see if the queue is empty.
// It returns true if the queue is empty, otherwise it returns false.

template <class myType>
const bool linkedQueue<myType>::isEmptyQueue() {
    return (count == 0);
}


// initializeStack
// =====================================
// Initializes (resets) the queue back to an empty state.

template <class myType>
void linkedQueue<myType>::initializeQueue() {
    queueNode<myType> *current;
    while (queueFront) {
        current = queueFront;
        queueFront = queueFront->link;
        delete current;
    }
}


// addItem
// =====================================
// Adds the passed value to the back of the queue (enqueue)
// Expands the queue to new nodes when necessary

template <class myType>
void linkedQueue<myType>::enqueue(const myType& newItem) {
    //check if the queue exists (first add)
    if (!queueFront) {
        queueFront = new queueNode<myType>;
        queueRear = queueFront;
        queueFront->link = NULL;
        queueFront->front = 0;
        queueFront->back = -1;
    }
    
    //check if queue is full
    if (queueRear->back == SIZE - 1) {
        //create the next node and link it
        queueNode<myType> *nextNode = new queueNode<myType>;
        queueRear->link = nextNode;
        nextNode->front = 0;
        nextNode->back = -1;
        queueRear = nextNode;
        
    }
    
    //enqueue the new element
    ++queueRear->back;
    queueRear->dataSet[queueRear->back] = newItem;
    ++count;
}

// front
// =====================================
// Function returns the first value in the queue

template <class myType>
myType linkedQueue<myType>::front() {
    return queueFront->dataSet[queueFront->front];
}

// back
// =====================================
// Function returns the last value in the queue

template <class myType>
myType linkedQueue<myType>::back() {
    return queueRear->dataSet[queueRear->back];
}


// deleteItem
// =====================================
// Removes the front item from the queue and returns nothing
// If the queue is empty, nothing will happen

template <class myType>
myType linkedQueue<myType>::dequeue() {
    if (isEmptyQueue())
        return NULL;
    
    myType temp = front();
    //remove the front value
    ++queueFront->front;
    --count;
    
    //check to see if queueFront is empty
    if (queueFront->front > queueFront->back) {
        //move to the next node
        queueNode<myType> *temp = queueFront;
        queueFront = queueFront->link;
        delete temp;
    }
    return temp;
}

// queueCount
// =====================================
// Returns the current count of the elements in the queue

template <class myType>
int linkedQueue<myType>::queueCount() {
    return count;
}

#endif	/* LINKEDQUEUE_H */