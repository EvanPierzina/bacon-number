/* 
 * Author: Evan Pierzina
 * 
 * baconNumber.h
 * ===========================================
 * This is an implementation for finding Bacon
 * Numbers while playing Six Degrees of Kevin Bacon.
 * It has four main functions:
 *  -Creating a graph with all actors and movies
 *   from a given database file with the format:
 *          <movie name>/<actor name>/<actor name>/.../<actor name>
 *          <movie name>/<actor name>/<actor name>/.../<actor name>
 *          <movie name>/<actor name>/<actor name>/.../<actor name>
 * 
 *  -Finding Bacon Numbers of a given actor in the graph
 *  -Listing all actors of a given movie
 *  -Listing all movies of a given actor
 * 
 */

#ifndef BACONNUMBER_H
#define	BACONNUMBER_H
#include <string>

using namespace std;

struct association;

struct vertex {
    string name;                        //movie or actor name
    vertex* hashChain = NULL;           //used to chain entries in the hash table
    association *associations = NULL;   //aka a linked list of vertices
    
    //BFS variables
    int dist;                           //distance from original node
    vertex* path = NULL;                //points to the vertex that it came from
};


struct association {
    vertex* associate = NULL;           //pointer to a movie/actor
    association* next = NULL;           //pointer to the next node in the linked list
};

class baconNumber {
public:
    baconNumber();
    virtual ~baconNumber();
    bool createGraph(const string);
    bool listActors (const string);
    bool listMovies (const string);
    bool reportBaconNumber(const string);
    int getMovieCount();
    int getActorCount();
    int getVertexCount();
    int getEdgeCount();
private:
    int hash(string);
    
    vertex **hashTable;                 //the mother of all data structures
    int movieCount;
    int actorCount;
    int edgeCount;
};

#endif	/* BACONNUMBER_H */

