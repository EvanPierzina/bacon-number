/* 
 * File:   main.cpp
 * Author: Evan
 *
 * Created on November 30, 2014, 8:20 PM
 */

#include <limits>
#include <cstdlib>
#include <string>
#include <iostream>
#include "baconNumber.h"

using namespace std;

int main(int argc, char** argv) {
    
    baconNumber BN;
    string fileName;
    string actor;
    string movie;
    int option = 1;

    // *****************************************************************
    //  Headers...

	string	stars, bars, dashes;
	stars.append(65, '*');
	bars.append(65, '=');
	dashes.append(40,'-');

	cout << "Six Degrees of Kevin Bacon" << endl;
	cout << endl;
        cout << bars << endl;
        
    // ------------------------------------------------------------------
    //  Check argument and create graph

        if (argc == 2) {
            fileName = argv[1];
            if (!BN.createGraph(fileName)) {
                cout << "Error, could not read data file specified." << endl;
                cout << "Please enter the data file you would like to use:" << endl;
                cin >> fileName;
            }
        }

        else {
            cout << "Please enter the data file you would like to use:" << endl;
            cin >> fileName;

            while (!BN.createGraph(fileName)) {
                cout << "Error, could not read data file specified." << endl;
                cout << "Please enter the data file you would like to use:" << endl;
                cin >> fileName;
            }
        }
    
    // ------------------------------------------------------------------
    // Prompt for options...
    
        while (option) {

            cout << "Choose an option 0-4:" << endl;
            cout << "0 - Terminate the program" << endl;
            cout << "1 - Find Bacon Number" << endl;
            cout << "2 - Movie Query" << endl;
            cout << "3 - Actor Query" << endl;
            cout << "4 - Show Graph Statistics" << endl;
            cout << "Option: ";
            cin >> option;
            cin.ignore(numeric_limits<std::streamsize>::max(), '\n');

            //  find bacon number
            if (option == 1) {
                cout << dashes << endl;
                cout << "Enter the name of an actor to find their bacon number:" << endl;
                cout << "Example: Bacon, Kevin" << endl << "Actor name: ";
                getline(cin, actor);
                cout << endl;
                if (!BN.reportBaconNumber(actor))
                    cout << "Error: actor not found in the graph." << endl;
            }

            //  movie query
            else if (option == 2) {
                cout << dashes << endl;
                cout << "Enter the name of a movie to find its actors (if it exists):" << endl;
                cout << "Example: Bambi (1942)" << endl << "Movie name: ";
                getline(cin, movie);
                cout << endl;
                if (!BN.listActors(movie))
                    cout << "Error: movie not found in the graph." << endl;
            }

            //  actor query
            else if (option == 3) {
                cout << dashes << endl;
                cout << "Enter the name of a actor to find their movies (if it exists):" << endl;
                cout << "Example: Pitt, Brad" << endl << "Actor name: ";
                getline(cin, actor);
                cout << endl;
                if (!BN.listMovies(actor))
                    cout << "Error: actor not found in the graph." << endl;
            }

            //  graph statistics (actor/movie count/vertices/edges)
            else if (option == 4) {
                cout << dashes << endl;
                cout << "Graph Statistics for " << fileName << ":" << endl;
                cout << "Actors: " << BN.getActorCount() << endl;
                cout << "Movies: " << BN.getMovieCount() << endl;
                cout << "Vertices: " << BN.getVertexCount() << endl;
                cout << "Edges: " << BN.getEdgeCount() << endl;
            }

            else if (option)
                cout << "Error: invalid option." << endl;

            cout << bars << endl;
        }
    
    // *****************************************************************
    //  All done.

            cout << endl << stars << endl <<
                    "Game Over, thank you for playing." << endl;
            
    return 0;
}

