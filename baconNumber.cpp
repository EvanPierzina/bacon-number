/* 
 * Author: Evan Pierzina
 * 
 * baconNumber.cpp
 * ===========================================
 * This is an implementation for finding Bacon
 * Numbers while playing Six Degrees of Kevin Bacon.
 * It has four main functions:
 *  -Creating a graph with all actors and movies
 *   from a given database file with the format:
 *          <movie name>/<actor name>/<actor name>/.../<actor name>
 *          <movie name>/<actor name>/<actor name>/.../<actor name>
 *          <movie name>/<actor name>/<actor name>/.../<actor name>
 * 
 *  -Finding Bacon Numbers of a given actor in the graph
 *  -Listing all actors of a given movie
 *  -Listing all movies of a given actor
 * 
 */

#include <fstream>
#include <iostream>
#include <sstream>
#include "baconNumber.h"
#include "linkedQueue.h"

// If memory is not a concern, the tableSize
// should be a number that results in few
// collisions while also not being too large
// for the initialization process.
#define tableSize 7367197

// Match the name formatting used in the database file
// ex: "Kevin Bacon" or "Bacon, Kevin"
#define centerOfTheUniverse "Bacon, Kevin"

// baconNumber()
// =====================================
// Default constructor, initialized class
// variables.
baconNumber::baconNumber() {
    hashTable = new vertex*[tableSize];
    
    //initialization required
    for (int i = 0; i < tableSize; i++)
        hashTable[i] = NULL;
    
    movieCount = 0;
    actorCount = 0;
    edgeCount = 0;
}

// ~baconNumber()
// =====================================
// Destructor, releases memory used by the
// class variables and initializes them.
baconNumber::~baconNumber() {
    movieCount = 0;
    actorCount = 0;
    edgeCount = 0;
    
    for (int i = 0; i < tableSize; i++) {
        if (hashTable[i]) {
            //entry exists, delete anything chained to it
            vertex *current = hashTable[i];
            while (current->hashChain) {
                vertex* temp = current;
                current = current->hashChain;
                delete temp;
            }
            delete current;
        }
    }
    delete hashTable;
}

// createGraph(const string)
// =====================================
// Reads in a database file of movies and
// actors, then creates a graph connecting
// actors to the movies they were in, and
// movies to the actors that were in them.
//
// Returns false if the file cannot be
// opened, otherwise returns true.
// Requires a file name.
bool baconNumber::createGraph(const string fileName) {
    //check file
    ifstream file(fileName.c_str());
    if (!file)
        return false;
    
    string line;
    string movieName;
    string actorName;
        
    //read in file
    while (getline(file, line)) {
        stringstream data(line);
        
        // ADD THE MOVIE TO TABLE AND LIST
        // -------------------------------------------
        getline(data, movieName, '/');
        
        //add to hash table
        vertex* movie = new vertex();
        movieCount++;
        movie->name = movieName;
        movie->dist = 0;
        int hashValue = hash(movieName);
        bool isDuplicateActor = false;
        
        //collision check
        if (hashTable[hashValue]) {
                //check for a current chain
                vertex* temp = hashTable[hashValue];
                //if there's a chain, find the end of it
                while (temp->hashChain) {
                    temp = temp->hashChain;
                }
                //at the end of the chain, add movie to it 
                temp->hashChain = movie;
        }
        
        else
            hashTable[hashValue] = movie;
        
        // ADD THE ACTORS TO TABLE AND LIST
        // -------------------------------------------
        movie->associations = new association(); //holds the association list for the current movie
        association *currentMovieAssociation = movie->associations; //holds the current position of the association list
        
        while (getline(data, actorName, '/')) {

            hashValue = hash(actorName);
            //check if actor exists
            if (hashTable[hashValue]) {
                vertex* currentVertex = hashTable[hashValue];
                isDuplicateActor = (hashTable[hashValue]->name == actorName);
                if (!isDuplicateActor) {
                    //check throughout the chain as well
                    while (currentVertex->hashChain && !isDuplicateActor) {
                        currentVertex = currentVertex->hashChain;
                        isDuplicateActor = (currentVertex->name == actorName);
                    }
                    //if the above loop broke out because the chain ended
                    if (!isDuplicateActor) {
                        //actor does not exist, add them to the chain, associate them with the current movie
                        vertex* actor = new vertex();
                        actorCount++;
                        actor->name = actorName;
                        actor->dist = 0;
                        actor->associations = new association();
                        actor->associations->associate = movie;
                        edgeCount++;

                        currentVertex->hashChain = actor;    //update hash table
                        
                        //associate actor with the movie
                        if (!currentMovieAssociation->associate) {
                            //first actor listed in the movie
                            currentMovieAssociation->associate = actor;
                        }
                        else {
                            //create new node, link to it, associate the actor
                            currentMovieAssociation->next = new association();
                            currentMovieAssociation = currentMovieAssociation->next;
                            currentMovieAssociation->associate = actor;
                        }
                    }
                }
                
                //if the actor already exists
                if (isDuplicateActor) {
                    //add the movie to the end of their associations
                    association *tempAssociation = currentVertex->associations;
                    while (tempAssociation->next) {
                        tempAssociation = tempAssociation->next;
                    }
                    //at the end of linked list, create a new association an link to it
                    tempAssociation->next = new association();
                    tempAssociation->next->associate = movie;
                    edgeCount++;
                    //associate actor with the movie
                    if (!currentMovieAssociation->associate) {
                        //first actor listed in the movie
                        currentMovieAssociation->associate = currentVertex;
                    }
                    else {
                        //create new node, link to it, associate the actor
                        currentMovieAssociation->next = new association();
                        currentMovieAssociation = currentMovieAssociation->next;
                        currentMovieAssociation->associate = currentVertex;
                    }
                }
            }

            else {
                //new actor -> add them to the hash table, associate them with the movie

                vertex* actor = new vertex();
                actorCount++;
                actor->name = actorName;
                actor->dist = 0;
                actor->associations = new association();
                actor->associations->associate = movie;
                edgeCount++;

                hashTable[hashValue] = actor;   //update hash table
                        
                //associate actor with the movie
                if (!currentMovieAssociation->associate) {
                    //first actor listed in the movie
                    currentMovieAssociation->associate = actor;
                }
                else {
                    //create new node, link to it, associate the actor
                    currentMovieAssociation->next = new association();
                    currentMovieAssociation = currentMovieAssociation->next;
                    currentMovieAssociation->associate = actor;
                }
            }
        }
    }
    return true;
}

// listActors(const string)
// =====================================
// Lists all the actors of a given movie.
// Requires a movie name.
// Returns false if the movie is not in
// the graph, otherwise returns true.
bool baconNumber::listActors(const string movieName) {
    int hashValue = hash(movieName);
    
    //check if the movie exists
    if (!hashTable[hashValue])
        return false;
    
    //traverse hash index until movie is found
    vertex *temp = hashTable[hashValue];
    while (temp->name != movieName) {
        if (temp->hashChain)
            temp = temp->hashChain;
        else
            return false;
    }
    
    //movie found, list the actors
    association *tempAssociations = temp->associations;
    cout << "The movie " << movieName << " has the following cast:" << endl;
    while (tempAssociations->next) {
        cout << tempAssociations->associate->name << endl;
        tempAssociations = tempAssociations->next;
    }
    cout << tempAssociations->associate->name << endl;
    
    return true;
}

// listMovies(const string)
// =====================================
// Lists all the movies a given actor was in.
// Requires an actor name.
// Returns false if the actor is not in
// the graph, otherwise returns true.
bool baconNumber::listMovies(const string actorName) {
    int hashValue = hash(actorName);
    
    //traverse hash index until name is found
    if (!hashTable[hashValue])
        return false;
    
    vertex *temp = hashTable[hashValue];
    while (temp->name != actorName) {
        if (temp->hashChain)
            temp = temp->hashChain;
        else
            return false;
    }
    
    //name found, list the movies
    association *tempAssociations = temp->associations;
    cout << actorName << " was in:" << endl;
    while (tempAssociations->next) {
        cout << tempAssociations->associate->name << endl;
        tempAssociations = tempAssociations->next;
    }
    cout << tempAssociations->associate->name << endl;
    
    return true;
}

// reportBaconNumber(const string)
// =====================================
// Reports the bacon number of a given actor
// and also outputs the path between them
// and Kevin Bacon.
// Requires an actor name.
// Returns false if the actor is not in
// the graph, otherwise returns true.
bool baconNumber::reportBaconNumber(const string actorName) {
    
    //find starting point (passed actor)
    int actorHashValue = hash(actorName);
    
    //traverse hash index until name is found
    if (!hashTable[actorHashValue])
        return false;
    
    vertex *start = hashTable[actorHashValue];
    while (start->name != actorName) {
        if (start->hashChain)
            start = start->hashChain;
        else
            return false;
    }
    
    //find ending point (Kevin Bacon)
    int baconHashValue = hash(centerOfTheUniverse);
    
    //traverse hash index until name is found
    if (!hashTable[baconHashValue])
        return false;
    
    vertex *kevinBacon = hashTable[baconHashValue];
    while (kevinBacon->name != centerOfTheUniverse) {
        if (kevinBacon->hashChain)
            kevinBacon = kevinBacon->hashChain;
        else {
            cout << "Kevin Bacon was not found in the graph" << endl;
            return false;
        }
    }
    
    //names found, start the search
    linkedQueue<vertex*> queue;
    queue.initializeQueue();
    queue.enqueue(start);
    start->dist = 0;
    
    while (!queue.isEmptyQueue()) {
        vertex *v = queue.dequeue();
        if (v->dist > 12) {
            cout << actorName << " has a bacon number beyond 6." << endl;
            return true;
        }
        
        else if (v == kevinBacon) {
            cout << start->name << " has a bacon number of " << v->dist / 2 << "." << endl; 
            
            //construct output string
            if (v->dist) {
                vertex *pathVertex = kevinBacon;

                cout << pathVertex->name << " appeared in ";
                pathVertex = pathVertex->path;
                while (pathVertex != start) {
                    if (pathVertex->dist % 2) //indicates movie
                        cout << pathVertex->name << " with ";
                    else
                        cout << pathVertex->name << " who appeared in ";
                    pathVertex = pathVertex->path;
                }
                cout << pathVertex->name << endl;
            }
            else {
                cout << "What did you expect?" << endl;
            }
            return true;
        }
        
        else {
            while (v->associations->next) {
                vertex *w = v->associations->associate;
                if (!(w->path)) { //prevent looping back
                    w->dist = v->dist + 1;
                    w->path = v;

                    queue.enqueue(w); //enqueues the actor or movie
                }
                v->associations = v->associations->next; //move to the next associate of v
            }
            
            vertex *w = v->associations->associate;
            if (!(w->path)) { //prevent looping back
                    w->dist = v->dist + 1;
                    w->path = v;

                    queue.enqueue(w); //enqueues the actor or movie
                }
        }
    }
    
    cout << actorName << " has a bacon number of infinity.";
    return true;
}

// getMovieCount()
// =====================================
// Returns the number of movies in the graph.
int baconNumber::getMovieCount() {
    return movieCount;
}

// getActorCount()
// =====================================
// Returns the number of actors in the graph.
int baconNumber::getActorCount() {
    return actorCount;
}

// getVertexCount()
// =====================================
// Returns the number of vertices in the graph.
int baconNumber::getVertexCount() {
    return movieCount + actorCount;
}

// getEdgeCount()
// =====================================
// Returns the number of edges in the graph.
int baconNumber::getEdgeCount() {
    return edgeCount;
}

// hash(string)
// =====================================
// Generates an index for the hashTable
// based on the name given.
// Returns the hash index.
int baconNumber::hash(string value) {
    unsigned int hashValue = 5381;
    for (int i = 0; i < value.length(); i++)
        hashValue = value[i] + ((hashValue << 5) + hashValue);
    
    return hashValue % tableSize;
}