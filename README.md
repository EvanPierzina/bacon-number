# README #

This program finds an actor's Bacon number; that is, how closely an actor is related to Kevin Bacon in terms of movies they have appeared in. You may have heard this called Six Degrees of Kevin Bacon.

Input Files
===========
The program requires a specifically formatted text file to populate the graphs for movies and actors. Each line should start with the movie name and follow with the actors' names, all separated with a '/' as shown below:

<MOVIE NAME>/<ACTOR NAME 1>/<ACTOR NAME 2>/.../<ACTOR NAME>

An example of how a line might look:

```
#!text

Agent Who Stole Christmas, The (2001)/Hoffman, Rick (I)/Bellner, Matt/Rubens, Jack (I)
```

As an example, a (very) large set of movies and actors is included in the file named largeSet.txt.

Compile
=======
From the command line, run make in the directory where the source files are located (ex: C:/bacon-number> make).
If you are having trouble compiling, a pre-made executable is already included (baconNumber.exe)

Usage
=======
Run the executable and follow the on-screen prompts.
Be sure to spell everything exactly the way it would appear in the file being used, it is case-sensitive.
In the provided text file, an example of an actor's name would look like this: 'Jolie, Angelina'

Changing the Actor from Kevin Bacon
=====
If you decide to use a different format for names, or if you want to see the degrees of another actor, you must change the line of code in baconNumber.cpp that looks like this:

```
#!C++

// Match the name formatting used in the database file
// ex: "Kevin Bacon" or "Bacon, Kevin"
#define centerOfTheUniverse "Bacon, Kevin"
```